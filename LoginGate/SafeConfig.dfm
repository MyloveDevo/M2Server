object frmSafeConfig: TfrmSafeConfig
  Left = 289
  Top = 143
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #23433#20840#35774#32622
  ClientHeight = 199
  ClientWidth = 390
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 377
    Height = 185
    ActivePage = TabSheet2
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #22522#26412#21442#25968
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object GroupBoxNet: TGroupBox
        Left = 3
        Top = 3
        Width = 345
        Height = 113
        Caption = #22522#26412#35774#32622
        TabOrder = 0
        object LabelGateIPaddr: TLabel
          Left = 8
          Top = 20
          Width = 76
          Height = 13
          Caption = #30331#38470#22120#29256#26412#21495':'
        end
        object EditSoftVersionDate: TEdit
          Left = 90
          Top = 17
          Width = 97
          Height = 21
          Hint = #23458#25143#31471#29256#26412#26085#26399#35774#32622#65292#27492#26085#26399#25968#23383#24517#39035#19982#23458#25143#31471#19978#30340#26085#26399#26631#35782#19968#33268#65292#21542#21017#36827#20837#28216#25103#26102#20250#25552#31034#29256#26412#38169#35823#12290#28857#20445#23384#25353#38062#21518#29983#25928#12290
          TabOrder = 0
          Text = '20130109'
        end
      end
      object ButtonOK: TButton
        Left = 296
        Top = 128
        Width = 65
        Height = 25
        Caption = #30830#23450'(&O)'
        TabOrder = 1
        OnClick = ButtonOKClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = #23494#30721#35774#32622
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label2: TLabel
        Left = 8
        Top = 11
        Width = 60
        Height = 13
        Caption = #30331#38470#23494#30721#65306
      end
      object EdtPassword: TEdit
        Left = 74
        Top = 8
        Width = 287
        Height = 21
        Hint = #35774#32622#23458#25143#31471#26657#39564#23494#30721#65292#27492#23494#30721#23558#34987#21152#23494#21518#20256#36755
        MaxLength = 12
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = 'Password'
      end
      object MemoWarningMsg: TMemo
        Left = 8
        Top = 36
        Width = 353
        Height = 85
        Lines.Strings = (
          ''
          '                 '#30331#38470#22120#29256#26412#38169#35823#65292#35831#26356#26032#33267#26368#26032#29256#26412#12290#65281)
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object Button1: TButton
        Left = 296
        Top = 128
        Width = 65
        Height = 25
        Caption = #30830#23450'(&O)'
        TabOrder = 2
      end
    end
  end
end

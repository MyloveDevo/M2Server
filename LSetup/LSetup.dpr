program LSetup;

uses
  Forms,
  FrmMain in 'FrmMain.pas' {FormMain},
  FrmEditServer in 'FrmEditServer.pas' {FormEditServer},
  MD5Unit in '..\Common\MD5Unit.pas',
  FrmEditUpData in 'FrmEditUpData.pas' {FormEditUpData},
  HUtil32 in '..\Common\HUtil32.pas',
  FrmLogin in 'FrmLogin.pas' {FormLogin},
  SCShare in '..\Common\SCShare.pas',
  GeneralCommon in '..\Common\GeneralCommon.pas',
  DES in '..\Common\DES.pas',
  MyCommon in '..\MyCommon\MyCommon.pas',
  MakeLoginCommon in '..\Common\MakeLoginCommon.pas',
  Share in '..\GameLogin\Share.pas',
  Grobal2 in '..\Common\Grobal2.pas',
  EDcodeEx in '..\Common\EDcodeEx.pas';

{$R *.res}
begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'LSetup';
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormEditUpData, FormEditUpData);
  Application.CreateForm(TFormEditServer, FormEditServer);
  Application.Run;
end.

unit MakeLoginCommon;

interface

type
  //登陆器自身信息
  TRecinfo = record
    Link: string[255];
    Link2: string[255];
    Name: string[100];
    sBulidVer: string[20]; // 版本号 必须和网关对应，否则进入游戏会提示版本错误
    sGatePass: string[20]; // 网关密码，必须配合客户端使用，否则会提示数据错误，无法进入游戏
    nRunCount: integer; // 允许双开几个客户端
  end;
  pTRecinfo = ^TRecinfo;

  //网关自身信息
  TRunGateInfo =record
    sBulidVer: string[20]; // 网关版本号，必须和登陆器配套否则无法进入游戏 
    sGatePass: string[20]; // 网关密码，必须配合客户端使用，否则会提示数据错误，无法进入游戏
  end;
  pTRunGateInfo =^TRunGateInfo;
  
implementation

end.

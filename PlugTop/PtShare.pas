unit PtShare;

interface
  uses
  Windows, Messages, SysUtils;
const
  tPlugtop=9;
  GS_QUIT = 2000;
  SG_FORMHANDLE = 1000; //服务器HANLD
  SG_STARTNOW = 1001; //正在启动服务器...
  SG_STARTOK = 1002; //启动完成...


var

g_dwGameCenterHandle: THandle;
procedure SendGameCenterMsg(wIdent: Word; sSendMsg: string);



implementation
     procedure SendGameCenterMsg(wIdent: Word; sSendMsg: string);
var
  SendData: TCopyDataStruct;
  nParam: Integer;
begin
  nParam := MakeLong(Word(tPlugtop), wIdent);
  SendData.cbData := Length(sSendMsg) + 1;
  GetMem(SendData.lpData, SendData.cbData);
  StrCopy(SendData.lpData, PChar(sSendMsg));
  SendMessage(g_dwGameCenterHandle, WM_COPYDATA, nParam, Cardinal(@SendData));
  FreeMem(SendData.lpData);
end;
end.

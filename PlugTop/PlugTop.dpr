program PlugTop;

uses
  Forms,
  FrmMain in 'FrmMain.pas' {FrmPlugTop},
  Grobal2 in '..\Common\Grobal2.pas',
  MudUtil in '..\Common\MudUtil.pas',
  MyCommon in '..\MyCommon\MyCommon.pas',
  HUtil32 in '..\Common\HUtil32.pas',
  PtShare in 'PtShare.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'PlugTop';
  Application.CreateForm(TFrmPlugTop, FrmPlugTop);
  Application.Run;
end.

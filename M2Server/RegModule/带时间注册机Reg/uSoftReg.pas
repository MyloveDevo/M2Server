unit uSoftReg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Regware2,iedcode, ComCtrls;

type
  TfrmMain = class(TForm)
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edtPawnName: TEdit;
    edtName: TEdit;
    edtCode: TEdit;
    btnGen: TButton;
    btnReg: TButton;
    btnUnReg: TButton;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    edtRegistered: TEdit;
    edtOrg2: TEdit;
    edtName2: TEdit;
    edtCode2: TEdit;
    edtDaysLeft: TEdit;
    edtExpired: TEdit;
    edtCheckTamper: TEdit;
    edtDays: TEdit;
    Label12: TLabel;
    edtAuthCode: TEdit;
    Label14: TLabel;
    edtRegCode: TEdit;
    Label15: TLabel;
    edtAuthCode2: TEdit;
    DateTimePicker1: TDateTimePicker;
    RegwareIIII: TRegwareII;

    Label13: TLabel;
    procedure btnGenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnRegClick(Sender: TObject);
    procedure btnUnRegClick(Sender: TObject);
  private
    { Private declarations }
    procedure FillLocalInfo;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;
  RegwareII:TRegwareII;
implementation

{$R *.dfm}
const
SEED_D=$45B7;
function GenerateCode(LicenseID: string; ExpireDate: TDate): string;
var
  S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, S13, STMP: string;
  V1, V2, V3, I1, I2: integer;
  Month, Day, Year: Word;
begin
  Result := '';
  if (Length(LicenseID) > 50) or (Length(LicenseID) < 5) then Exit;

  //---- Encode Expiration Date ------------------------------------------
  if ExpireDate > 0 then
  begin
    DecodeDate(ExpireDate, Year, Month, Day);
  end else
  begin
    // Make expiration date of 0/00/0000 (no expiration)
    DecodeDate(0, Year, Month, Day);
  end;

  V1 := integer(Month) xor SEED_D and $000F;
  V2 := integer(Day)   xor SEED_D and $00FF;
  V3 := integer(Year)  xor SEED_D and $0FFF;

  S2 := IntToHex(V1, 1);  // Encoded month

  STMP := IntToHex(V2, 4);  // Encoded day
  S10 := STMP[3];
  S8 := STMP[4];

  STMP := IntToHex(V3, 4);  // Encoded year
  S4 := STMP[2];
  S6 := STMP[3];
  S12 := STMP[4];

  //---- Encode Segment 1 (chars #1,3,5) ---------------------------------
  V3 := Length(LicenseID);
  V1 := Ord(LicenseID[1]) + Ord(LicenseID[2]) + Ord(LicenseID[Trunc(V3 / 2)]) +
        Ord(LicenseID[V3]) + Ord(LicenseID[V3 - 1]);
  //V1 := V1 div 4;
  V2 := (188657688 mod V1) and $00FF;
  STMP := IntToHex(V2, 4);
  S1 := STMP[3];
  S3 := STMP[4];

  V1 := Length(LicenseID);
  if V1 > 16 then STMP := '0000' else STMP := IntToHex(V1, 4);
  S5 := STMP[4];

  //---- Encode Segment 2 (chars #7,9,11) --------------------------------
  V1 := 0;
  for I1 := 1 to Length(LicenseID) do
    V1 := V1 + Ord(LicenseID[I1]);
  V1 := ((V1 shl 4 xor 47006716) and $0FFF);
  STMP := IntToHex(V1, 4);
  S7 := STMP[2];
  S9 := STMP[3];
  S11 := STMP[4];

  //------------- Check segment 3 (chars #13...) ----------------------------
  V2 := 0;
  STMP := '';
  for I1 := 1 to Length(LicenseID) do
    V2 := V2 + Ord(LicenseID[I1]);
  V2 := V2 * ($7FFFFFF div V2);
  I2 := 31;
  for I1 := 1 to 32 do
  begin
    // Rotates the bits in FSeed3 through shifting,
    // but bits that fall off are moved to the other end
    V3 := (141819309 shl I1) or (141819309 shr I2) and $7FFFFFFF;
    V3 := abs(V3);
    if V3 > V2 then
      V1 := V3 mod V2
    else
      V1 := V2 mod V3;
    STMP := STMP + IntToHex($00000FFF and V1, 3);
    Dec(I2);
  end;
  // Take only the characters needed
  //Delete(STMP, RegCodeSize - 11, Length(STMP) - RegCodeSize - 11);
  S13 := Copy(STMP, 1, 20 - 12);

  Result := S1+S2+S3+S4+S5+S6+S7+S8+S9+S10+S11+S12+S13;
end;
procedure TfrmMain.btnGenClick(Sender: TObject);
var
ExpireDate: TDate;
begin
ExpireDate:=DateTimePicker1.Date;
  RegwareII.AuthCode := Trim(edtAuthCode.Text);
  edtCode.Text := RegwareII.CalculateCode(edtPawnName.Text);
  edtRegCode.Text := RegwareII.CalculateCodeEx(edtPawnName.Text, IDeCodeString(edtAuthCode2.Text))
  +'-'+GenerateCode('4552524630',ExpireDate);
end;

procedure TfrmMain.FillLocalInfo;
begin
  edtDaysLeft.Text := IntToStr(RegwareII.DaysLeft);
  if RegwareII.Expired then
    edtExpired.Text := 'True'
  else
    edtExpired.Text := 'False';
  if RegwareII.CheckTamper then
    edtCheckTamper.Text := 'True'
  else
    edtCheckTamper.Text := 'False';
  edtDays.Text := IntToStr(RegwareII.Days);
  if RegwareII.Registered then
    edtRegistered.Text := 'True'
  else
    edtRegistered.Text := 'False';
  if Trim(RegwareII.GetIdeDiskSerialNumberEx) = '' then
    edtAuthCode.Text := RegwareII.AuthCode
  else
    edtAuthCode.Text := Trim(RegwareII.GetIdeDiskSerialNumberEx);
  edtOrg2.Text := RegwareII.Organization;
  edtName2.Text := RegwareII.License;
  edtCode2.Text := RegwareII.RegCode;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
//-注册开始
  RegwareII:=TRegwareII.Create(Owner);
  RegwareII.MaxChars:=100;
  RegwareII.Seed:=123456789;
  RegwareII.ProgGUID:='{7FF02D23-3021-410E-88EB-57834625BB7F}';
//  RegwareII.AuthCode:='IHZPGLY99';
//-注册结束
  FillLocalInfo;
end;

procedure TfrmMain.btnRegClick(Sender: TObject);
var
  s: string;
begin
  if not RegwareII.DoRegistration(edtPawnName.Text, edtName.Text, edtCode.Text) then
  begin
    s := '输入注册码不正确，请检查！';
    Application.MessageBox(PChar(s), PChar('输入错误'), MB_OK + MB_ICONINFORMATION);
  end
  else
  begin
    s := '注册成功！' + #13 + '注册信息为：' +
      #13 + '使用者名称：' + RegwareII.License +
      #13 + '注册人姓名：' + RegwareII.Organization +
      #13 + '注册码：' + RegwareII.RegCode +
      #13 + '感谢您对我们的支持！';
    Application.MessageBox(PChar(s), PChar('注册成功'), MB_OK + MB_ICONINFORMATION);
  end;
  FillLocalInfo;
end;

procedure TfrmMain.btnUnRegClick(Sender: TObject);
begin
  RegwareII.SetUnregistered;
  FillLocalInfo;
end;

end.


{       WAK Productions Presents:                               }
{       TRegware    version 2.1                                 }
{       Copyright ?001 - WAK Productions                       }
{       ---------------------------------                       }
{       Programmed by Winston Kotzan                            }
{       Email: waksoftware@hotmail.com                          }
{       Website URL: http://www.wakproductions.com/                        }
{
This program helps developers of shareware include a timebomb/registration
for their program.  The end-user will enter their a name representing their
license, and a serial number (kinda like Winzip).  If their registration info.
is correct, then the program becomes registered.  TRegware hides data in the Windows
registry to check if program was registered.

Version 2 of TRegware uses a different, more secure method of encoding
data in the registry.  Unfortunately, this makes version 1 of TRegware
incompatible with version 2.  To provide backwards compatibility, the
class name for TRegware in version 2 has been changed to TRegwareII.
}
(*
Updates
-------
06-28-01  --  Version 2.1  (no backwards compatibility)
-Fixed bug that caused program to improperly cut down registration codes that
 were longer RegCodeSize, causing the last number in some codes to be incorrect.
-New feature that detects when user sets clock backwards.  If the user tries
 setting his computer's clock to an earlier time, program will consider itself
 'expired'.  When the user puts his clock back within the remaining x days
 of his evaluation, the program will continue to operate as normal.


Quick Reference
---------------
Here is a sumamary of the usage of the variables in TRegInfo:

License -  Name of the user who registered program
Organization - Organization or company of user who registered program
ExpireTime - TDateTime value of when the program will expire
  (decimal portion is ignored)
LastCountDown - Number of days left of evaluation time, recorded when the program
  was last shut down (used to make sure that user does not set clock backwards)
RegVersion - Version of TRegware used for registry (for future expansion)
RegCode - After the program has been registered, this value contains the serial
  number derived from the License value
ExpX - Not in use - provided for future expansion



End-User License Agreement
--------------------------
By using this software, you are agreeing to the following terms:

        1. WAK Productions and any employees will not be responsible for
           any damages, misuse, vertigo, or any other unfortunate accidents
           related to this product.

        2. Despite the many precautions made, there is no guarantee that this
           product will make your shareware program invulnerable to 'cracks',
           'hacks', or any other type of manipulation to bypass or deceive
           the shareware timebomb.

        2. The author of this software mantains the right to freely modify whole
           or any portion of this product in the next release.

        3. Any modifications must be marked as such, with some credit given to
           its original creator.

        4. You will not claim ownership of this product.

        5. This product may only be redistributed under the conditions that
           it comes in a duplicate of its original package, including
           all accompanied files, documentation, etc.
*)


unit Regware2;

interface

uses WinTypes, WinProcs, Messages, SysUtils, Classes, Controls,
  Forms, Graphics, Registry, ComObj, Dialogs,HardwareInfo;

type
  TRegInfo = record
    License, Organization, RegCode, Exp1: string[100];
    RegVersion: string[10];
    LastCountDown, Exp2, Exp3: integer;
    ExpireTime, Exp4: TDateTime;
  end;

  TRegwareII = class(TComponent)
  private
      { Private fields of TRegwareII }
    FCheckTamper: boolean;
    FDays: Integer;
    FLastCountDown: integer;
    FExpireTime: TDateTime;
    FLicense: string;
    FMaxChars: integer;
    FMinChars: integer;
    FOrganization: string;
    FProgGUID: string;
    FRegCode: string;
    FRegCodeSize: integer;
    FSeed: Int64;
    FTimebomb: Boolean;
    FOnClockChange: TNotifyEvent;
    FOnExpire: TNotifyEvent;
    FOnNagScreen: TNotifyEvent;
    FRegVersion: string;
    FAuthCode: string;

      { Private methods of TRegwareII }
        { Method to set variable and property values and create objects }
    procedure AutoInitialize;
        { Method to free any objects created by AutoInitialize }
    procedure AutoDestroy;
    function GetDaysLeft: Integer;
    function GetRegistered: Boolean;
        { Checks if user set clock backwards }
    function CheckClockTampered: boolean;
    function GetExpired: boolean;
    procedure CheckVariablesSet;
    procedure LoadRegistryValues;
    procedure SaveRegistryValues;
    procedure SetMaxChars(MaxChars: integer);
    procedure SetMinChars(MinChars: integer);
    procedure SetSeed(Seed: Int64);


  protected
    procedure Expire; virtual;
    procedure ShowNag; virtual;
    procedure Loaded; override;
    procedure ClockChange; virtual;

  public
      { Calculates the registration code based on the LicenseName variable. }
    function CalculateCode(LicenseName: string): string;
    //用来计算注册码的，注册机使用！！！
    function CalculateCodeEx(LicenseName: string; sAuthCode: string): string;
      { Checks if the program's evaluation time has expired. True is returned
        if the time has run out, false if the evaluation period is still
        running. }
    function CheckExpired: boolean;
      { Checks the registry to see if the user has registered the program.
        True is returned if the program is registered, false otherwise. }
    function CheckRegistered: boolean;
      { Creates a new instance of TRegwareII }
    constructor Create(AOwner: TComponent); override;
      { Frees associated memory with TRegwareII.  Do not call Destroy directly,
        instead call the Free method }
    destructor Destroy; override;
      { Registers the program. LicenseName is the name of the person the program
        is being registered to.  Organization is optional, it can be the user's
        company. Organization is ignored if it is a null string.  RegCode is the
        registration code that goes with license.  To get RegCode, use the
        CalculateCode method.  Program will not be registered if serial is the
        incorrect registration code for LicenseName. }
    function DoRegistration(LicenseName, Organization, RegCode: string): boolean;
      { Resets the registry so that the program becomes unregistered. If the
        Timebomb property is true, then the timebomb is reset }
//    function GetIdeDiskSerialNumber: string; //张光勇添加，获取硬盘序列号
    function GetIdeDiskSerialNumberEx: string; //张光勇添加，获取硬盘序列号的运算后代码
    procedure SetUnregistered;

      { The number of days left (if unregistered) for the evaluation period.
        This property is equal to -1 if the program is registered or has passed the
        time limit. }
    property DaysLeft: Integer read GetDaysLeft;
      { Indicates whether the program has expired. }
    property Expired: boolean read GetExpired;
      { An identifying name for the user's paid license.  Usually this is the
        name of the person the program is licensed to. This property is only
        available after the program has been registered.  It is the same as the
        LicenseName parameter passed to DoRegistration() }
    property License: string read FLicense;
      { This is the name of the organization (if any) that the licensee belongs
        to.  This property is only available after the program has been registered.
        It will be the same as the Organization parameter passed to DoRegistration() }
    property Organization: string read FOrganization;
      { This is the registration code based on the License property.  Same
        as the RegCode parameter passed to DoRegistration().  This property is
        only available after the program has been registered }
    property RegCode: string read FRegCode;
      { Indicates whether the program has been registered. }
    property Registered: Boolean read GetRegistered;

  published
      { Event is sent after the component loads in memory.  This event means that
        tampering has been done with the system's clock.  Difference is how many
        days back the clock has been set. }
    property OnClockChange: TNotifyEvent read FOnClockChange write FOnClockChange;
      { Event sent on startup of program if program has expired }
    property OnExpire: TNotifyEvent read FOnExpire write FOnExpire;
      { Event sent on startup of program, indicating an appropriate time
        to show a nag screen.  See Demo1 for proper use. }
    property OnNagScreen: TNotifyEvent read FOnNagScreen write FOnNagScreen;
    property CheckTamper: boolean read FCheckTamper write FCheckTamper default true;
      { This is the number of days timebomb will run.  It is ignored if the timebomb is already running. }
    property Days: Integer read FDays write FDays default 30;
      { Maximum number of characters that the License name can be }
    property MaxChars: integer read FMaxChars write SetMaxChars default 25;
      { Minimum number of characters that License name can be }
    property MinChars: integer read FMinChars write SetMinChars default 3;
      { The GUID string indicating the registry key where timebomb and registration
        information is stored.  If the programmer forgets to set this property
        to a valid GUID string, then a message box shows up making sure that
        the programmer is reminded. For more information on GUID's, look up TGUID
        in your Delphi online help file. }
    property ProgGUID: string read FProgGUID write FProgGUID;
      { A number used in generating the registration code.  Changing this number
        will yield different registration codes. If you forget to set this property
        to a value between 1000 and 2^63, then you WILL BE REMINDED when you
        try running the program. }
    property Seed: Int64 read FSeed write SetSeed default 0;
      { The number of digits you would like the registration code to be.  Setting
        this to 12 will have TRegwareII generate a registration code 12 digits long }
    property RegCodeSize: integer read FRegCodeSize write FRegCodeSize default 12;
      { This property contains the version number of the TRegwareII component you
        are using.  (Not applicable to version 1.0)
      property RegVersion: integer read FRegVersion;
      { Set this to true if you want TRegwareII to cause the program to stop functioning
        after a certain number of days.  If this is set to false, the program
        will not expire. }
    property Timebomb: Boolean read FTimebomb write FTimebomb default True;
    //张光勇添加，如果硬盘序列号为空，则AuthCode为一固定值
    property AuthCode: string read FAuthCode write FAuthCode;

  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('SoftReg控件', [TRegwareII]);
end;

{ Checks for a valid GUID string }

function ValidateGUID(GUIDStr: string): boolean;
  { Check for hex character }
  function IsHex(AlphaNumChar: char): boolean;
  begin
    if ((Ord(AlphaNumChar) >= 48) and (Ord(AlphaNumChar) <= 57)) or
      ((Ord(AlphaNumChar) >= 65) and (Ord(AlphaNumChar) <= 70)) then
      Result := true
    else
      Result := false;
  end;

var
  i: integer;
begin
  Result := false;
  if Length(GUIDStr) <> 38 then Exit; // Not proper length
  for i := 1 to Length(GUIDStr) do
  begin
    // Check for valid letters and numbers
    if (i <> 1) and (i <> 10) and (i <> 15) and (i <> 20) and (i <> 25) and (i <> 38) then
    begin
      if not IsHex(GUIDStr[i]) then Exit;
    end else
      // Check for valid brackets
      if (i = 1) and (GUIDStr[i] <> '{') then Exit
      else if (i = 38) and (GUIDStr[i] <> '}') then Exit
          // Hyphens
      else if (i = 10) or (i = 15) or (i = 20) or (i = 25) then
        if (GUIDStr[i] <> '-') then Exit;
  end;
  Result := true; // Ok, if all the above conditions checked;
end;

{------------------ TRegwareII methods ------------------}

{ Method to set variable and property values and create objects }

procedure TRegwareII.AutoInitialize;
begin
  FCheckTamper := true;
  FDays := 30;
  FMaxChars := 25;
  FMinChars := 3;
  FTimebomb := True;
end; { of AutoInitialize }

{ Method to free any objects created by AutoInitialize }

procedure TRegwareII.AutoDestroy;
begin
end;

function TRegwareII.GetDaysLeft: Integer;
begin
  if CheckClockTampered then // User tampered with clock
    Result := 0 // Consider program as expired
  else if (not Registered) and (Timebomb = true) then
  begin
    Result := Trunc(FExpireTime - Date);
    if Result < 0 then Result := 0;
  end
  else // Clock not tampered and program registered
    Result := -1;
end;

function TRegwareII.GetRegistered: Boolean;
begin
  Result := true; //CheckRegistered;
end;

{ Calculates the registration code to register program }

function TRegwareII.CalculateCode(LicenseName: string): string;
var
  i: integer;
  SumChar: Int64;
  HDSN, HDSN1, HDSN2: string;
  LocalLicenseName: string;
begin
////////////////////
//张光勇修改，将获取的硬盘序列号代码加入到注册码中
  HDSN := Trim(GetIdeDiskSerialNumberEx);
  if HDSN = '' then
    LocalLicenseName := LicenseName
  else
  begin //HDSN分为２部分，一部分加到前面，剩下的加到后面
    i := Length(HDSN);
    HDSN1 := Copy(HDSN, 1, (i div 2));
    HDSN2 := Copy(HDSN, (i div 2) + 1, i - (i div 2));
    LocalLicenseName := HDSN1 + LicenseName + HDSN2;
  end;
////////////////////
  SumChar := 0;
  if (Length(LicenseName) > FMaxChars) or (Length(LicenseName) < FMinChars) then
  begin
    Result := ''; // LicenseName did not fit length requirements
    Exit;
  end;
  i := Length(LocalLicenseName);
  //取所有的字符
  while (i <= Length(LocalLicenseName)) and (i > 0) do
  begin
    SumChar := SumChar + FSeed - 1113 mod Ord(LocalLicenseName[i]);
    Dec(i);
  end;
  if FRegCodeSize <= 0 then
    Result := IntToHex(SumChar, 0)
  else
  begin
    Result := IntToHex(SumChar, FRegCodeSize);
    Delete(Result, FRegCodeSize + 1, Length(Result) - FRegCodeSize); //将多余FRegCodeSize后面的部分去掉
  end;
end;

{ Checks if time has expired }

function TRegwareII.CheckExpired: boolean;
begin
  if GetDaysLeft = 0 then Result := true else Result := false;
end;

{ Checks if program is registered }

function TRegwareII.CheckRegistered: boolean;
begin
  if (FRegCode <> '') and (FLicense <> '') then
  begin
    if CompareStr(FRegCode, CalculateCode(FLicense)) = 0 then
    begin
      Result := true; // Reg Values check... program registered
    end else
    begin
      Result := false; // Reg Values invalid... program not registered
      // Clear the false values
      FRegCode := '';
      FLicense := '';
      FOrganization := '';
    end;
  end else Result := false; // Values not there... program not registered
end;

procedure TRegwareII.SetMaxChars(MaxChars: integer);
begin
  if (MaxChars > 100) or (MaxChars < MinChars) then
    raise Exception.Create('Please enter a number between ' +
      IntToStr(FMinChars) + ' and 100')
  else
    FMaxChars := MaxChars;
end;

constructor TRegwareII.Create(AOwner: TComponent);
var
Info:THardwareInfo;//防止 通用 key
begin
  inherited Create(AOwner);
  { AutoInitialize sets the initial values of variables and properties }
  if  Trim(Info.GetIDEDiskDriveInfo('C')) = '' then
    FAuthCode := Trim(Info.GetCPUInfo(1)+Info.GetMACAddress()+Info.GetIDEDiskSerialNumber+Info.GetIDEDiskDriveInfo('C'))
  {
  CConfig.MACAddress:=Info.GetMACAddress();
CConfig.IDEDiskSerialNumber:=Info.GetIDEDiskSerialNumber;
CConfig.IDEDiskDriveInfo:=Info.GetIDEDiskDriveInfo('C'); 好像这可以用
CConfig.CPUInfo1:=Info.GetCPUInfo(1);
CConfig.CPUInfo2:=Info.GetCPUInfo(2);
CConfig.CPUInfo3:=Info.GetCPUInfo(3);
  }
  else
  FAuthCode := Trim(Info.GetIDEDiskDriveInfo('C')+Info.GetCPUInfo(2)+Info.GetCPUInfo(3)+Info.GetMACAddress());
  AutoInitialize;
  FRegCodeSize := 12;
end;

destructor TRegwareII.Destroy;
begin
  SaveRegistryValues;
  AutoDestroy;
  inherited Destroy;
end;

function TRegwareII.DoRegistration(LicenseName, Organization, RegCode: string): boolean;
begin
  Result := true;
  Exit;
  if (Length(LicenseName) > FMaxChars) or (Length(LicenseName) < FMinChars) or (Length(RegCode) = 0) then
  begin
    Result := false;
    Exit;
  end;
  RegCode := UpperCase(RegCode); // Registration codes are not case sensitive
  if CompareStr(CalculateCode(LicenseName), RegCode) <> 0 then Result := false
  else begin
    FLicense := LicenseName;
    FOrganization := Organization;
    FRegCode := RegCode;
    SaveRegistryValues;
    Result := true;
  end;
end;

procedure TRegwareII.SetUnregistered;
begin
  FLicense := '';
  FOrganization := '';
  FRegCode := '';
  if Timebomb then
  begin
    FExpireTime := Now + FDays;
    FLastCountDown := FDays;
  end else
  begin
    FExpireTime := 0;
    FLastCountDown := FDays;
  end;
  SaveRegistryValues;
end;

procedure TRegwareII.Expire;
begin
  if Assigned(FOnExpire) then FOnExpire(Self);
end;

procedure TRegwareII.ShowNag;
begin
  if Assigned(FOnNagScreen) then FOnNagScreen(Self);
end;

procedure TRegwareII.Loaded;
var
Info:THardwareInfo;//防止 通用 key
begin
  inherited;
  if Trim(Info.GetIDEDiskDriveInfo('C')) = '' then
    FAuthCode := Trim(Info.GetCPUInfo(1)+Info.GetMACAddress()+Info.GetIDEDiskSerialNumber+Info.GetIDEDiskDriveInfo('C'))
  else
    FAuthCode := Trim(Info.GetIDEDiskDriveInfo('C')+Info.GetCPUInfo(2)+Info.GetCPUInfo(3)+Info.GetMACAddress());

  CheckVariablesSet;
  LoadRegistryValues;
  if (not Registered) and (FExpireTime = 0) then
    SetUnregistered
  else if CheckClockTampered then
    ClockChange
  else if not Registered then
  begin
    ShowNag;
    if CheckExpired = true then
      Expire;
  end;
end;

procedure TRegwareII.ClockChange;
begin
  if Assigned(FOnClockChange) then FOnClockChange(Self);
end;

function TRegwareII.CheckClockTampered: boolean;
var
  DateDifference: integer; // Difference between expiration date and now
begin
  Result := false;
  if FCheckTamper = true then
  begin
    DateDifference := Trunc(FExpireTime - Date);
    if DateDifference > FLastCountDown then // User set clock backwards
      Result := true;
  end;
end;

function TRegwareII.GetExpired: boolean;
begin
  Result := CheckExpired;
end;

procedure TRegwareII.SetSeed(Seed: Int64);
begin
  if (Seed > 1000) and (FSeed <> Seed) then FSeed := Seed;
end;


procedure TRegwareII.SaveRegistryValues;
var
  RegData: TRegInfo;
  Registry: TRegistry;
begin
  if ValidateGUID(FProgGUID) = false then Exit;

  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_LOCAL_MACHINE;
    Registry.OpenKey('\Software\CLASSES\CLSID\' + FProgGUID + '\InprocServer32', true);
    with RegData do
    begin
      License := FLicense;
      Organization := FOrganization;
      RegCode := FRegCode;
      RegVersion := '2.00';

      if not Registered then
      begin
        ExpireTime := FExpireTime;
        if CheckClockTampered = false then
          LastCountDown := Trunc(ExpireTime - Date)
        else
          LastCountDown := FLastCountDown; // Only save this value if clock is correct
      end else
      begin
        ExpireTime := 0;
        LastCountDown := 0;
      end; // if..then..else

    end; // with RegData do
    Registry.WriteBinaryData('ThreadingModel', RegData, SizeOf(RegData));
    Registry.CloseKey;
  finally
    Registry.Free;
  end; // try..finally }
end;

procedure TRegwareII.SetMinChars(MinChars: integer);
begin
  if (MinChars > FMaxChars) or (MinChars < 3) then
    raise Exception.Create('Please enter a number between 3 and ' +
      IntToStr(FMaxChars));
end;

procedure TRegwareII.LoadRegistryValues;
var
  RegData: TRegInfo;
  Registry: TRegistry;
begin
  if ValidateGUID(FProgGUID) = false then Exit;

  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_LOCAL_MACHINE;
    Registry.OpenKey('\Software\CLASSES\CLSID\' + FProgGUID + '\InprocServer32', true);
    if Registry.ValueExists('ThreadingModel') then
    begin
      Registry.ReadBinaryData('ThreadingModel', RegData, SizeOf(RegData));
      with RegData do
      begin
        FLicense := License;
        FOrganization := Organization;
        FRegCode := RegCode;
        FExpireTime := ExpireTime;
        FRegVersion := RegVersion;
        FLastCountDown := LastCountDown;
      end;
    end else
    begin
      SetUnregistered;
    end;
    Registry.CloseKey;
  finally
    Registry.Free;
  end; // try..finally
end;

procedure TRegwareII.CheckVariablesSet;
begin
  if ValidateGUID(FProgGUID) = false then
    ShowMessage('Mr. Programmer forgot to set the ProgGUID property to a ' +
      'valid GUID string!  Tsk, Tsk...');
  if FSeed = 0 then
    ShowMessage('Mr. Programmer forgot to set the Seed property to a ' +
      'valid integer between 1000 and 2^63!  Tsk, Tsk...');
end;

(*
function TRegwareII.GetIdeDiskSerialNumber: string;
{得到硬盘序列号过程}
type
  TSrbIoControl = packed record
    HeaderLength: ULONG;
    Signature: array[0..7] of Char;
    Timeout: ULONG;
    ControlCode: ULONG;
    ReturnCode: ULONG;
    Length: ULONG;
  end;
  SRB_IO_CONTROL = TSrbIoControl;
  PSrbIoControl = ^TSrbIoControl;
  TIDERegs = packed record
    bFeaturesReg: Byte; // Used for specifying SMART "commands".
    bSectorCountReg: Byte; // IDE sector count register
    bSectorNumberReg: Byte; // IDE sector number register
    bCylLowReg: Byte; // IDE low order cylinder value
    bCylHighReg: Byte; // IDE high order cylinder value
    bDriveHeadReg: Byte; // IDE drive/head register
    bCommandReg: Byte; // Actual IDE command.
    bReserved: Byte; // reserved. Must be zero.
  end;
  IDEREGS = TIDERegs;
  PIDERegs = ^TIDERegs;
  TSendCmdInParams = packed record
    cBufferSize: DWORD;
    irDriveRegs: TIDERegs;
    bDriveNumber: Byte;
    bReserved: array[0..2] of Byte;
    dwReserved: array[0..3] of DWORD;
    bBuffer: array[0..0] of Byte;
  end;
  SENDCMDINPARAMS = TSendCmdInParams;
  PSendCmdInParams = ^TSendCmdInParams;
  TIdSector = packed record
    wGenConfig: Word;
    wNumCyls: Word;
    wReserved: Word;
    wNumHeads: Word;
    wBytesPerTrack: Word;
    wBytesPerSector: Word;
    wSectorsPerTrack: Word;
    wVendorUnique: array[0..2] of Word;
    sSerialNumber: array[0..19] of Char;
    wBufferType: Word;
    wBufferSize: Word;
    wECCSize: Word;
    sFirmwareRev: array[0..7] of Char;
    sModelNumber: array[0..39] of Char;
    wMoreVendorUnique: Word;
    wDoubleWordIO: Word;
    wCapabilities: Word;
    wReserved1: Word;
    wPIOTiming: Word;
    wDMATiming: Word;
    wBS: Word;
    wNumCurrentCyls: Word;
    wNumCurrentHeads: Word;
    wNumCurrentSectorsPerTrack: Word;
    ulCurrentSectorCapacity: ULONG;
    wMultSectorStuff: Word;
    ulTotalAddressableSectors: ULONG;
    wSingleWordDMA: Word;
    wMultiWordDMA: Word;
    bReserved: array[0..127] of Byte;
  end;
  PIdSector = ^TIdSector;
const
  IDE_ID_FUNCTION = $EC;
  IDENTIFY_BUFFER_SIZE = 512;
  DFP_RECEIVE_DRIVE_DATA = $0007C088;
  IOCTL_SCSI_MINIPORT = $0004D008;
  IOCTL_SCSI_MINIPORT_IDENTIFY = $001B0501;
  DataSize = sizeof(TSendCmdInParams) - 1 + IDENTIFY_BUFFER_SIZE;
  BufferSize = SizeOf(SRB_IO_CONTROL) + DataSize;
  W9xBufferSize = IDENTIFY_BUFFER_SIZE + 16;
var
  hDevice: THandle;
  cbBytesReturned: DWORD;
  pInData: PSendCmdInParams;
  pOutData: Pointer; // PSendCmdOutParams
  Buffer: array[0..BufferSize - 1] of Byte;
  srbControl: TSrbIoControl absolute Buffer;

  procedure ChangeByteOrder(var Data; Size: Integer);
  var
    ptr: PChar;
    i: Integer;
    c: Char;
  begin
    ptr := @Data;
    for i := 0 to (Size shr 1) - 1 do
    begin
      c := ptr^;
      ptr^ := (ptr + 1)^;
      (ptr + 1)^ := c;
      Inc(ptr, 2);
    end;
  end;

begin
  Result := '';
  FillChar(Buffer, BufferSize, #0);
  if Win32Platform = VER_PLATFORM_WIN32_NT then
  begin // Windows NT, Windows 2000
    // Get SCSI port handle
    hDevice := CreateFile('\\.\Scsi0:',
      GENERIC_READ or GENERIC_WRITE,
      FILE_SHARE_READ or FILE_SHARE_WRITE,
      nil, OPEN_EXISTING, 0, 0);
    if hDevice = INVALID_HANDLE_VALUE then Exit;
    try
      srbControl.HeaderLength := SizeOf(SRB_IO_CONTROL);
      System.Move('SCSIDISK', srbControl.Signature, 8);
      srbControl.Timeout := 2;
      srbControl.Length := DataSize;
      srbControl.ControlCode := IOCTL_SCSI_MINIPORT_IDENTIFY;
      pInData := PSendCmdInParams(PChar(@Buffer)
        + SizeOf(SRB_IO_CONTROL));
      pOutData := pInData;
      with pInData^ do
      begin
        cBufferSize := IDENTIFY_BUFFER_SIZE;
        bDriveNumber := 0;
        with irDriveRegs do
        begin
          bFeaturesReg := 0;
          bSectorCountReg := 1;
          bSectorNumberReg := 1;
          bCylLowReg := 0;
          bCylHighReg := 0;
          bDriveHeadReg := $A0;
          bCommandReg := IDE_ID_FUNCTION;
        end;
      end;
      if not DeviceIoControl(hDevice, IOCTL_SCSI_MINIPORT,
        @Buffer, BufferSize, @Buffer, BufferSize,
        cbBytesReturned, nil) then Exit;
    finally
      CloseHandle(hDevice);
    end;
  end else
  begin // Windows 95 OSR2, Windows 98
    hDevice := CreateFile('\\.\SMARTVSD', 0, 0, nil,
      CREATE_NEW, 0, 0);
    if hDevice = INVALID_HANDLE_VALUE then Exit;
    try
      pInData := PSendCmdInParams(@Buffer);
      pOutData := @pInData^.bBuffer;
      with pInData^ do
      begin
        cBufferSize := IDENTIFY_BUFFER_SIZE;
        bDriveNumber := 0;
        with irDriveRegs do
        begin
          bFeaturesReg := 0;
          bSectorCountReg := 1;
          bSectorNumberReg := 1;
          bCylLowReg := 0;
          bCylHighReg := 0;
          bDriveHeadReg := $A0;
          bCommandReg := IDE_ID_FUNCTION;
        end;
      end;
      if not DeviceIoControl(hDevice, DFP_RECEIVE_DRIVE_DATA,
        pInData, SizeOf(TSendCmdInParams) - 1, pOutData,
        W9xBufferSize, cbBytesReturned, nil) then Exit;
    finally
      CloseHandle(hDevice);
    end;
  end;
  with PIdSector(PChar(pOutData) + 16)^ do
  begin
    ChangeByteOrder(sSerialNumber, SizeOf(sSerialNumber));
    SetString(Result, sSerialNumber, SizeOf(sSerialNumber));
  end;
end;
   *)
function TRegwareII.GetIdeDiskSerialNumberEx: string;
var
  IDESN: string;
  i: Integer;
  SumChar: Int64;
begin
  SumChar := 0;
  IDESN := FAuthCode;//
//  showmessage(FAuthCode);
  i := Length(IDESN);
  while (i <= Length(IDESN)) and (i > 0) do
  begin
//  showmessage(inttostr(SumChar)+'+'+inttostr(FSeed)+'-1113 mod '+inttostr(Ord(IDESN[i])));
    if Ord(IDESN[i])>0 then begin
    SumChar := SumChar + FSeed - 1113 mod Ord(IDESN[i]);
    end;
    Dec(i);
  end;
  if FRegCodeSize <= 0 then
    Result := IntToHex(SumChar, 0)
  else
  begin
    Result := IntToHex(SumChar, FRegCodeSize);
    Delete(Result, FRegCodeSize + 1, Length(Result) - FRegCodeSize);
  end;
end;

function TRegwareII.CalculateCodeEx(LicenseName, sAuthCode: string): string;
var
  i: integer;
  SumChar: Int64;
  HDSN, HDSN1, HDSN2: string;
  LocalLicenseName: string;
begin
  HDSN := sAuthCode;
  if HDSN = '' then
    LocalLicenseName := LicenseName
  else
  begin
    i := Length(HDSN);
    HDSN1 := Copy(HDSN, 1, (i div 2));
    HDSN2 := Copy(HDSN, (i div 2) + 1, i - (i div 2));
    LocalLicenseName := HDSN1 + LicenseName + HDSN2;
  end;
  SumChar := 0;
  if (Length(LicenseName) > FMaxChars) or (Length(LicenseName) < FMinChars) then
  begin
    Result := '';
    Exit;
  end;
  i := Length(LocalLicenseName);
  while (i <= Length(LocalLicenseName)) and (i > 0) do
  begin
    SumChar := SumChar + FSeed - 1113 mod Ord(LocalLicenseName[i]);
    Dec(i);
  end;
  if FRegCodeSize <= 0 then
    Result := IntToHex(SumChar, 0)
  else
  begin
    Result := IntToHex(SumChar, FRegCodeSize);
    Delete(Result, FRegCodeSize + 1, Length(Result) - FRegCodeSize);
  end;
end;

end.


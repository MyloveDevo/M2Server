unit uSoftReg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Regware2,iedcode;

type
  TfrmMain = class(TForm)
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edtPawnName: TEdit;
    edtName: TEdit;
    edtCode: TEdit;
    btnGen: TButton;
    btnReg: TButton;
    btnUnReg: TButton;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    edtRegistered: TEdit;
    edtOrg2: TEdit;
    edtName2: TEdit;
    edtCode2: TEdit;
    edtDaysLeft: TEdit;
    edtExpired: TEdit;
    edtCheckTamper: TEdit;
    edtDays: TEdit;
    Label12: TLabel;
    edtAuthCode: TEdit;
    Label14: TLabel;
    edtRegCode: TEdit;
    Label15: TLabel;
    edtAuthCode2: TEdit;
    RegwareIIII: TRegwareII;
    procedure btnGenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnRegClick(Sender: TObject);
    procedure btnUnRegClick(Sender: TObject);
  private
    { Private declarations }
    procedure FillLocalInfo;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;
  RegwareII:TRegwareII;
implementation

{$R *.dfm}

procedure TfrmMain.btnGenClick(Sender: TObject);
begin
  RegwareII.AuthCode := Trim(edtAuthCode.Text);
  edtCode.Text := RegwareII.CalculateCode(edtPawnName.Text);
  edtRegCode.Text := RegwareII.CalculateCodeEx(edtPawnName.Text, IDeCodeString(edtAuthCode2.Text));
end;

procedure TfrmMain.FillLocalInfo;
begin
  edtDaysLeft.Text := IntToStr(RegwareII.DaysLeft);
  if RegwareII.Expired then
    edtExpired.Text := 'True'
  else
    edtExpired.Text := 'False';
  if RegwareII.CheckTamper then
    edtCheckTamper.Text := 'True'
  else
    edtCheckTamper.Text := 'False';
  edtDays.Text := IntToStr(RegwareII.Days);
  if RegwareII.Registered then
    edtRegistered.Text := 'True'
  else
    edtRegistered.Text := 'False';
  if Trim(RegwareII.GetIdeDiskSerialNumberEx) = '' then
    edtAuthCode.Text := RegwareII.AuthCode
  else
  edtAuthCode.Text := {IEnCodeString}(Trim(RegwareII.GetIdeDiskSerialNumberEx));
//  ShowMessage(RegwareII.AuthCode);
  edtOrg2.Text := RegwareII.Organization;
  edtName2.Text := RegwareII.License;
  edtCode2.Text := RegwareII.RegCode;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
//-注册开始
  RegwareII:=TRegwareII.Create(Owner);
  RegwareII.MaxChars:=100;
  RegwareII.Seed:=123456789;
  RegwareII.ProgGUID:='{7FF02D23-3021-410E-88EB-57834625BB7F}';
//  RegwareII.AuthCode:='IHZPGLY99';
//-注册结束
  FillLocalInfo;
end;

procedure TfrmMain.btnRegClick(Sender: TObject);
var
  s: string;
begin
  if not RegwareII.DoRegistration(edtPawnName.Text, edtName.Text, edtCode.Text) then
  begin
    s := '输入注册码不正确，请检查！';
    Application.MessageBox(PChar(s), PChar('输入错误'), MB_OK + MB_ICONINFORMATION);
  end
  else
  begin
    s := '注册成功！' + #13 + '注册信息为：' +
      #13 + '使用者名称：' + RegwareII.License +
      #13 + '注册人姓名：' + RegwareII.Organization +
      #13 + '注册码：' + RegwareII.RegCode +
      #13 + '感谢您对我们的支持！';
    Application.MessageBox(PChar(s), PChar('注册成功'), MB_OK + MB_ICONINFORMATION);
  end;
  FillLocalInfo;
end;

procedure TfrmMain.btnUnRegClick(Sender: TObject);
begin
  RegwareII.SetUnregistered;
  FillLocalInfo;
end;

end.


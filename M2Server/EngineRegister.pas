unit EngineRegister;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, svMain, IniFiles, RzButton, Mask, RzEdit,StrUtils;
const
SEED_D=$45B7;
type
  TFrmRegister = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EditUserName: TRzEdit;
    EditRegisterName: TRzEdit;
    EditRegisterCode: TRzEdit;
    RzBitBtnRegister: TRzBitBtn;
    procedure RzBitBtnRegisterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Open();
  end;

var
  FrmRegister: TFrmRegister;

implementation
uses M2Share, SDK,uConfig,HardwareInfo,iedcode;
{$R *.dfm}
function SplitString(const source, ch: string): TStringList;
var
  temp, t2: string;
  i: integer;
begin
  result := TStringList.Create;
  temp := source;
  i := pos(ch, source);
  while i <> 0 do
  begin
    t2 := copy(temp, 0, i - 1);
    if (t2 <> '') then
      result.Add(t2);
    delete(temp, 1, i - 1 + Length(ch));
    i := pos(ch, temp);
  end;
  result.Add(temp);
end;
procedure TFrmRegister.FormCreate(Sender: TObject);
begin
caption:=caption+RegwareII.License;//注册公司
EditUserName.text:=RegwareII.Organization;//用户注册名字
EditRegisterName.text:=IEnCodeString(RegwareII.GetIdeDiskSerialNumberEx);//机器代码
EditRegisterCode.Text:=RegwareII.RegCode;//这里是key
end;

procedure TFrmRegister.Open();
begin
  ShowModal;
end;
    function HexToInt(hex:string):cardinal;  
    const cHex='0123456789ABCDEF';  
    var mult,i,loop:integer;  
    begin  
          result:=0;  
          mult:=1;  
          for loop:=length(hex)downto 1 do  
          begin
           i:=pos(hex[loop],cHex)-1;  
           if (i<0) then i:=0;  
           inc(result,(i*mult));
           mult:=mult*16;  
          end;  
    end;  
      
    function jiemi(const S:widestring):widestring;
    var  
      i: Integer;  
      FKey: Integer;  
      str:cardinal;  
      tmpstr:string;  
      len:integer;  
    begin  
          FKey :=  HexToInt(leftstr(s,2));
          tmpstr:=rightstr(s,length(s)-2);
          len:=HexToInt(leftstr(tmpstr,2));
          tmpstr:=rightstr(tmpstr,length(tmpstr)-2);
          for i:=1 to Length(tmpstr) do  
          begin  
            str:=HexToInt(copy(tmpstr,0,2));  
            tmpstr:=rightstr(tmpstr,length(tmpstr)-2);
           Result := Result+Chr(Ord(str) xor fkey xor i);
          end;  
          result:=leftstr(result,len);
    end;

function CheckCodeExpiration(RegCode: string): TDate;
var
  S1, S2, S3: string;
  I1, I2, I3: integer;
  Month, Day, Year: Word;
begin
    Result := 0;
    // The expiration date is encoded in characters 2, 4, 6, 8, 10, and 12
    S1 := RegCode[2];  // character #2 contains the month
    S2 := RegCode[10] + RegCode[8];  // characters #10 and #8 contain the day
    S3 := RegCode[4] + RegCode[6] + RegCode[12];  // chars #4, 6, 12 contain the year

    I1 := StrToIntDef('$' + S1, $FFFF);
    I2 := StrToIntDef('$' + S2, $FFFF);
    I3 := StrToIntDef('$' + S3, $FFFF);
    if (I1 or I2 or I3) = $FFFF then Exit;  // Failed on conversion

    Month :=   Word(I1 xor SEED_D and $000F);
    Day   :=   Word(I2 xor SEED_D and $00FF);
    Year  :=   Word(I3 xor SEED_D and $0FFF);

    Result := EncodeDate(Year, Month, Day);
end;
procedure TFrmRegister.RzBitBtnRegisterClick(Sender: TObject);
var
Info:THardwareInfo;
s: string;
ss:TStringList;
stext:string;
tvt,timer:TDate;
key:string;
begin
tvt:=Now;
stext:=EditRegisterCode.Text;
ss:=SplitString(stext,'-');
key:=ss[0];
timer:=CheckCodeExpiration(ss[1]);//时间解密
CConfig.LBSURL:=ss[1];
 if timer>tvt then
 begin
//第一个公司名字  //第二个用户名字    第三个key
  if not RegwareII.DoRegistration('我的引擎我做主', EditUserName.Text,key) then
  begin
    s := '输入注册码不正确，请检查！';
    Application.MessageBox(PChar(s), PChar('输入错误'), MB_OK + MB_ICONINFORMATION);
  end
  else
  begin
    s:='注册成功';
    Application.MessageBox(PChar(s), PChar('注册成功'), MB_OK + MB_ICONINFORMATION);
    close;
  end;
 end;
CConfig.UserName:=EditUserName.Text;//第二个用户名字
CConfig.RegisterCode:=EditRegisterCode.Text;//第三个key
CConfig.RegisterName:=EditRegisterName.Text;//机器码
{CConfig.MACAddress:=Info.GetMACAddress();
CConfig.IDEDiskSerialNumber:=Info.GetIDEDiskSerialNumber;
CConfig.IDEDiskDriveInfo:=Info.GetIDEDiskDriveInfo('C');
CConfig.CPUInfo1:=Info.GetCPUInfo(1);
CConfig.CPUInfo2:=Info.GetCPUInfo(2);
CConfig.CPUInfo3:=Info.GetCPUInfo(3);  }
end;

end.
